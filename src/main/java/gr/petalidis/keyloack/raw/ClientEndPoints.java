/*
 *
 *  *     Copyright 2020 Nikos Petalidis @ https://www.petalidis.gr
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *          http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package gr.petalidis.keyloack.raw;

public enum ClientEndPoints {
    SECRET( "ed41a1ec-3030-4f64-aa15-640cc616c1ea"),
    REDIRECT("http://localhost:8081/cb"),
    ID("keycloak-client"),
    ;

    private final String label;

    ClientEndPoints(String label) {
        this.label = label;
    }

    public final String label()
    {
        return label;
    }
}
