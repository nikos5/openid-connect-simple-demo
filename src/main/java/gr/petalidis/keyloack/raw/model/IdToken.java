/*
 *
 *  *     Copyright 2020 Nikos Petalidis @ https://www.petalidis.gr
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *          http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package gr.petalidis.keyloack.raw.model;

import com.auth0.jwk.JwkException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import gr.petalidis.keyloack.raw.PublicKeyClient;

import java.net.MalformedURLException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

public class IdToken {

    public static String validate(String base64encoded) throws JwkException, MalformedURLException {

        String[] split = base64encoded.split("\\.");

        byte[] decodedBytes = Base64.getUrlDecoder().decode(split[1]);
        String payload = new String(decodedBytes);

        String header = new String(Base64.getUrlDecoder().decode(split[0]));

        String signature = new String(Base64.getUrlDecoder().decode(split[2]));

        DecodedJWT decode = JWT.decode(base64encoded);

        PublicKey publicKey = PublicKeyClient.getPublicKey(decode.getKeyId());

        Algorithm algorithm = Algorithm.RSA256((RSAPublicKey)publicKey,null);
            algorithm.verify(decode);
            //We can get info on many claims
           // decode.getClaims() will return the claims present in the token
            // e.g. decode.getClaim("email") will return the user's email'
        return decode.getClaim("email").asString();

    }
}
