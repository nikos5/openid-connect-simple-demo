/*
 *
 *  *     Copyright 2020 Nikos Petalidis @ https://www.petalidis.gr
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *          http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package gr.petalidis.keyloack.raw;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.PublicKey;

public class PublicKeyClient {


    private final static String certsEndpoint = OpenIdEndPoints.BASE_URL.url()+OpenIdEndPoints.CERT.url();

    public static PublicKey  getPublicKey(String kid) throws JwkException, MalformedURLException {
        JwkProvider provider = new UrlJwkProvider(new URL(certsEndpoint));
        Jwk jwk = provider.get(kid);

        return jwk.getPublicKey();
    }
}
