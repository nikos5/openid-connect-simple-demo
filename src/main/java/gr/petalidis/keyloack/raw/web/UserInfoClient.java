/*
 *
 *  *     Copyright 2020 Nikos Petalidis @ https://www.petalidis.gr
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *          http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package gr.petalidis.keyloack.raw.web;

import gr.petalidis.keyloack.raw.OpenIdEndPoints;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class UserInfoClient {
    private final static String userInfoEndpoint = OpenIdEndPoints.BASE_URL.url() +  OpenIdEndPoints.USERINFO.url();
    public static String getUserInfo(String accessToken)
    {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Bearer " + accessToken);


        ResponseEntity<String> response = restTemplate.exchange(
                userInfoEndpoint , HttpMethod.GET, new HttpEntity<>(headers),String.class);

        return response.getBody();

    }
}
