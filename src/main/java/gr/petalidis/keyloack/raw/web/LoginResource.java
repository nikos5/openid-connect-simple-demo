/*
 *
 *  *     Copyright 2020 Nikos Petalidis @ https://www.petalidis.gr
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *          http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package gr.petalidis.keyloack.raw.web;

import gr.petalidis.keyloack.raw.ClientEndPoints;
import gr.petalidis.keyloack.raw.OpenIdEndPoints;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginResource {

    @GetMapping
    public ResponseEntity<String> doLogin()
    {
        HttpHeaders responseHeaders = new HttpHeaders();

        //Obviously in a realistic environment nonce and state would not be statically created like this
        responseHeaders.set("Location",
                OpenIdEndPoints.BASE_URL.url()
        +       OpenIdEndPoints.AUTH.url()
        +        "?response_type=code&client_id=keycloak-client&redirect_uri="+ ClientEndPoints.REDIRECT.label()+"&scope=openid%20profile&state=321&nonce=123");
        return ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT)
                .headers(responseHeaders).build();
    }
}
