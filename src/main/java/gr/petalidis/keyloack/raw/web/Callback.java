/*
 *
 *  *     Copyright 2020 Nikos Petalidis @ https://www.petalidis.gr
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *          http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package gr.petalidis.keyloack.raw.web;

import com.auth0.jwk.JwkException;
import gr.petalidis.keyloack.raw.ClientEndPoints;
import gr.petalidis.keyloack.raw.OpenIdEndPoints;
import gr.petalidis.keyloack.raw.model.IdToken;
import gr.petalidis.keyloack.raw.model.OAuthResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.MalformedURLException;
import java.util.Base64;

@RestController
@RequestMapping(value="/cb")
public class Callback {
    private final String tokenEndpoint = OpenIdEndPoints.BASE_URL.url() + OpenIdEndPoints.TOKEN.url();

    @GetMapping
    public String login(
            @RequestParam("state") String state,
            @RequestParam("session_state") String session_state,
            @RequestParam("code") String code) throws JwkException, MalformedURLException {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.set("Content-Type", "application/x-www-form-urlencoded");
        String encodedString = Base64.getEncoder().encodeToString((ClientEndPoints.ID.label()+":"+ ClientEndPoints.SECRET.label()).getBytes());
        headers.set("Authorization", "Basic " + encodedString);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("code", code);
        map.add("grant_type","authorization_code");
        map.add("redirect_uri",ClientEndPoints.REDIRECT.label());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<OAuthResponse> response = restTemplate.postForEntity(
                tokenEndpoint, request , OAuthResponse.class);

        String email = IdToken.validate(response.getBody().getId_token());

        String accessToken = response.getBody().getAccess_token();

        String userInfo = UserInfoClient.getUserInfo(accessToken);
        return "email = " +email + "\n" + "userinfo=" + userInfo;
    }

}
